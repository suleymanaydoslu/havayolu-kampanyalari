<?php

namespace App\Controller\Web;

use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    /**
     * @Route("/", name="web_landing_index")
     */
    public function index()
    {
        $campaigns = $this->getDoctrine()->getManager()->getRepository(Campaign::class)->getAllActiveCampaings();

        return $this->render('landing/index.html.twig', [
            'controller_name' => 'LandingController',
            'campaigns' => $campaigns
        ]);
    }
}
